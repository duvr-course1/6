export function createForest(forestScene: Entity): void {

  const planetFloorOrange_01 = new Entity()
  planetFloorOrange_01.setParent(forestScene)
  const gltfShape_49 = new GLTFShape('models/forest/PlanetFloorOrange_01/PlanetFloorOrange_01.glb')
  planetFloorOrange_01.addComponentOrReplace(gltfShape_49)
  const transform_75 = new Transform({
    position: new Vector3(16, 0, 48),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  planetFloorOrange_01.addComponentOrReplace(transform_75)
  engine.addEntity(planetFloorOrange_01)

  const planetFloorOrange_01_2 = new Entity()
  planetFloorOrange_01_2.setParent(forestScene)
  planetFloorOrange_01_2.addComponentOrReplace(gltfShape_49)
  const transform_76 = new Transform({
    position: new Vector3(32, 0, 48),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  planetFloorOrange_01_2.addComponentOrReplace(transform_76)
  engine.addEntity(planetFloorOrange_01_2)
  
  const planetFloorOrange_01_4 = new Entity()
  planetFloorOrange_01_4.setParent(forestScene)
  planetFloorOrange_01_4.addComponentOrReplace(gltfShape_49)
  const transform_78 = new Transform({
    position: new Vector3(32, 0, 32),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  planetFloorOrange_01_4.addComponentOrReplace(transform_78)
  engine.addEntity(planetFloorOrange_01_4)
  
  const planetFloorOrange_01_5 = new Entity()
  planetFloorOrange_01_5.setParent(forestScene)
  planetFloorOrange_01_5.addComponentOrReplace(gltfShape_49)
  const transform_79 = new Transform({
    position: new Vector3(16, 0, 32),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  planetFloorOrange_01_5.addComponentOrReplace(transform_79)
  engine.addEntity(planetFloorOrange_01_5)
  
  const planetFloorOrange_01_6 = new Entity()
  planetFloorOrange_01_6.setParent(forestScene)
  planetFloorOrange_01_6.addComponentOrReplace(gltfShape_49)
  const transform_80 = new Transform({
    position: new Vector3(32, 0, 16),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  planetFloorOrange_01_6.addComponentOrReplace(transform_80)
  engine.addEntity(planetFloorOrange_01_6)
  
  const planetFloorOrange_01_7 = new Entity()
  planetFloorOrange_01_7.setParent(forestScene)
  planetFloorOrange_01_7.addComponentOrReplace(gltfShape_49)
  const transform_81 = new Transform({
    position: new Vector3(16, 0, 16),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  planetFloorOrange_01_7.addComponentOrReplace(transform_81)
  engine.addEntity(planetFloorOrange_01_7)

  const treeSycamore_02 = new Entity()
  treeSycamore_02.setParent(forestScene)
  const gltfShape_2 = new GLTFShape('models/forest/TreeSycamore_02/TreeSycamore_02.glb')
  treeSycamore_02.addComponentOrReplace(gltfShape_2)
  const transform_8 = new Transform({
    position: new Vector3(26, 0, 10),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  treeSycamore_02.addComponentOrReplace(transform_8)
  engine.addEntity(treeSycamore_02)
  
  const treeSycamore_01 = new Entity()
  treeSycamore_01.setParent(forestScene)
  const gltfShape_3 = new GLTFShape('models/forest/TreeSycamore_01/TreeSycamore_01.glb')
  treeSycamore_01.addComponentOrReplace(gltfShape_3)
  const transform_9 = new Transform({
    position: new Vector3(28, 0, 38),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  treeSycamore_01.addComponentOrReplace(transform_9)
  engine.addEntity(treeSycamore_01)
  
  const treeTall_01 = new Entity()
  treeTall_01.setParent(forestScene)
  const gltfShape_4 = new GLTFShape('models/forest/TreeTall_01/TreeTall_01.glb')
  treeTall_01.addComponentOrReplace(gltfShape_4)
  const transform_10 = new Transform({
    position: new Vector3(19, 0, 38),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  treeTall_01.addComponentOrReplace(transform_10)
  engine.addEntity(treeTall_01)
  
  const treeTall_02 = new Entity()
  treeTall_02.setParent(forestScene)
  const gltfShape_5 = new GLTFShape('models/forest/TreeTall_02/TreeTall_02.glb')
  treeTall_02.addComponentOrReplace(gltfShape_5)
  const transform_11 = new Transform({
    position: new Vector3(3.5, 0, 6),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  treeTall_02.addComponentOrReplace(transform_11)
  engine.addEntity(treeTall_02)
  
  const treeFir_01 = new Entity()
  treeFir_01.setParent(forestScene)
  const gltfShape_6 = new GLTFShape('models/forest/TreeFir_01/TreeFir_01.glb')
  treeFir_01.addComponentOrReplace(gltfShape_6)
  const transform_12 = new Transform({
    position: new Vector3(17.5, 0, 43),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  treeFir_01.addComponentOrReplace(transform_12)
  engine.addEntity(treeFir_01)
  
  const treeFir_02 = new Entity()
  treeFir_02.setParent(forestScene)
  const gltfShape_7 = new GLTFShape('models/forest/TreeFir_02/TreeFir_02.glb')
  treeFir_02.addComponentOrReplace(gltfShape_7)
  const transform_13 = new Transform({
    position: new Vector3(20.5, 0, 41.5),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1.5, 1)
  })
  treeFir_02.addComponentOrReplace(transform_13)
  engine.addEntity(treeFir_02)
  
  const treeSycamore_03 = new Entity()
  treeSycamore_03.setParent(forestScene)
  const gltfShape_8 = new GLTFShape('models/forest/TreeSycamore_03/TreeSycamore_03.glb')
  treeSycamore_03.addComponentOrReplace(gltfShape_8)
  const transform_14 = new Transform({
    position: new Vector3(28, 0, 42),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  treeSycamore_03.addComponentOrReplace(transform_14)
  engine.addEntity(treeSycamore_03)
  
  const shoreGrass_01 = new Entity()
  shoreGrass_01.setParent(forestScene)
  const gltfShape_9 = new GLTFShape('models/forest/ShoreGrass_01/ShoreGrass_01.glb')
  shoreGrass_01.addComponentOrReplace(gltfShape_9)
  const transform_15 = new Transform({
    position: new Vector3(15, 0, 28),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  shoreGrass_01.addComponentOrReplace(transform_15)
  engine.addEntity(shoreGrass_01)
  
  const junglePlant_01 = new Entity()
  junglePlant_01.setParent(forestScene)
  const gltfShape_10 = new GLTFShape('models/forest/JunglePlant_01/JunglePlant_01.glb')
  junglePlant_01.addComponentOrReplace(gltfShape_10)
  const transform_16 = new Transform({
    position: new Vector3(5.5, 0, 29),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(0.42264973081037416, 3.0320083016486947, 0.42264973081037416)
  })
  junglePlant_01.addComponentOrReplace(transform_16)
  engine.addEntity(junglePlant_01)
  
  const junglePlant_02 = new Entity()
  junglePlant_02.setParent(forestScene)
  const gltfShape_11 = new GLTFShape('models/forest/JunglePlant_02/JunglePlant_02.glb')
  junglePlant_02.addComponentOrReplace(gltfShape_11)
  const transform_17 = new Transform({
    position: new Vector3(5, 0, 38),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1.9310213512248176, 4.1687441125391755, 1.6622565942745595)
  })
  junglePlant_02.addComponentOrReplace(transform_17)
  engine.addEntity(junglePlant_02)
  
  const junglePlant_03 = new Entity()
  junglePlant_03.setParent(forestScene)
  const gltfShape_12 = new GLTFShape('models/forest/JunglePlant_03/JunglePlant_03.glb')
  junglePlant_03.addComponentOrReplace(gltfShape_12)
  const transform_18 = new Transform({
    position: new Vector3(3.5, 0, 28.5),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  junglePlant_03.addComponentOrReplace(transform_18)
  engine.addEntity(junglePlant_03)
  
  const plant_01 = new Entity()
  plant_01.setParent(forestScene)
  const gltfShape_13 = new GLTFShape('models/forest/Plant_01/Plant_01.glb')
  plant_01.addComponentOrReplace(gltfShape_13)
  const transform_19 = new Transform({
    position: new Vector3(23.5, 0, 3.5),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  plant_01.addComponentOrReplace(transform_19)
  engine.addEntity(plant_01)
  
  const bush_Fantasy_01 = new Entity()
  bush_Fantasy_01.setParent(forestScene)
  const gltfShape_14 = new GLTFShape('models/forest/Bush_Fantasy_01/Bush_Fantasy_01.glb')
  bush_Fantasy_01.addComponentOrReplace(gltfShape_14)
  const transform_20 = new Transform({
    position: new Vector3(9.5, 0, 39),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  bush_Fantasy_01.addComponentOrReplace(transform_20)
  engine.addEntity(bush_Fantasy_01)
  
  const bush_Fantasy_04 = new Entity()
  bush_Fantasy_04.setParent(forestScene)
  const gltfShape_15 = new GLTFShape('models/forest/Bush_Fantasy_04/Bush_Fantasy_04.glb')
  bush_Fantasy_04.addComponentOrReplace(gltfShape_15)
  const transform_21 = new Transform({
    position: new Vector3(6, 0, 34),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  bush_Fantasy_04.addComponentOrReplace(transform_21)
  engine.addEntity(bush_Fantasy_04)
  
  const bush_Fantasy_03 = new Entity()
  bush_Fantasy_03.setParent(forestScene)
  const gltfShape_16 = new GLTFShape('models/forest/Bush_Fantasy_03/Bush_Fantasy_03.glb')
  bush_Fantasy_03.addComponentOrReplace(gltfShape_16)
  const transform_22 = new Transform({
    position: new Vector3(9.5, 0, 41.5),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  bush_Fantasy_03.addComponentOrReplace(transform_22)
  engine.addEntity(bush_Fantasy_03)
  
  const bush_Fantasy_02 = new Entity()
  bush_Fantasy_02.setParent(forestScene)
  const gltfShape_17 = new GLTFShape('models/forest/Bush_Fantasy_02/Bush_Fantasy_02.glb')
  bush_Fantasy_02.addComponentOrReplace(gltfShape_17)
  const transform_23 = new Transform({
    position: new Vector3(6, 0, 34.5),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  bush_Fantasy_02.addComponentOrReplace(transform_23)
  engine.addEntity(bush_Fantasy_02)
  
  const mushrooms_03 = new Entity()
  mushrooms_03.setParent(forestScene)
  const gltfShape_18 = new GLTFShape('models/forest/Mushrooms_03/Mushrooms_03.glb')
  mushrooms_03.addComponentOrReplace(gltfShape_18)
  const transform_24 = new Transform({
    position: new Vector3(6, 0, 21),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  mushrooms_03.addComponentOrReplace(transform_24)
  engine.addEntity(mushrooms_03)
  
  const mushrooms_04 = new Entity()
  mushrooms_04.setParent(forestScene)
  const gltfShape_19 = new GLTFShape('models/forest/Mushrooms_04/Mushrooms_04.glb')
  mushrooms_04.addComponentOrReplace(gltfShape_19)
  const transform_25 = new Transform({
    position: new Vector3(3, 0, 28.5),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(3.8624870579266606, 5.73442276544257, 3.230402407389267)
  })
  mushrooms_04.addComponentOrReplace(transform_25)
  engine.addEntity(mushrooms_04)
  
  const tree_Forest_Green_03 = new Entity()
  tree_Forest_Green_03.setParent(forestScene)
  const gltfShape_20 = new GLTFShape('models/forest/Tree_Forest_Green_03/Tree_Forest_Green_03.glb')
  tree_Forest_Green_03.addComponentOrReplace(gltfShape_20)
  const transform_26 = new Transform({
    position: new Vector3(7, 0, 8),
    rotation: new Quaternion(0, 0.6343932841636454, 0, 0.7730104533627369),
    scale: new Vector3(0.6014884098135074, 0.7314723400390548, 1)
  })
  tree_Forest_Green_03.addComponentOrReplace(transform_26)
  engine.addEntity(tree_Forest_Green_03)
  
  const tree_01 = new Entity()
  tree_01.setParent(forestScene)
  const gltfShape_21 = new GLTFShape('models/forest/Tree_01/Tree_01.glb')
  tree_01.addComponentOrReplace(gltfShape_21)
  const transform_27 = new Transform({
    position: new Vector3(3.5, 0, 17),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  tree_01.addComponentOrReplace(transform_27)
  engine.addEntity(tree_01)
  
  const tree_Leafs_02 = new Entity()
  tree_Leafs_02.setParent(forestScene)
  const gltfShape_22 = new GLTFShape('models/forest/Tree_Leafs_02/Tree_Leafs_02.glb')
  tree_Leafs_02.addComponentOrReplace(gltfShape_22)
  const transform_28 = new Transform({
    position: new Vector3(27.5, 0, 6),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  tree_Leafs_02.addComponentOrReplace(transform_28)
  engine.addEntity(tree_Leafs_02)
  
  const tree_Leafs_01 = new Entity()
  tree_Leafs_01.setParent(forestScene)
  const gltfShape_23 = new GLTFShape('models/forest/Tree_Leafs_01/Tree_Leafs_01.glb')
  tree_Leafs_01.addComponentOrReplace(gltfShape_23)
  const transform_29 = new Transform({
    position: new Vector3(25, 0, 6),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  tree_Leafs_01.addComponentOrReplace(transform_29)
  engine.addEntity(tree_Leafs_01)
  
  const tree_Leafs_03 = new Entity()
  tree_Leafs_03.setParent(forestScene)
  const gltfShape_24 = new GLTFShape('models/forest/Tree_Leafs_03/Tree_Leafs_03.glb')
  tree_Leafs_03.addComponentOrReplace(gltfShape_24)
  const transform_30 = new Transform({
    position: new Vector3(5.5, 0, 40.5),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1.4180477757051655, 1)
  })
  tree_Leafs_03.addComponentOrReplace(transform_30)
  engine.addEntity(tree_Leafs_03)
  
  const vegetation_06 = new Entity()
  vegetation_06.setParent(forestScene)
  const gltfShape_25 = new GLTFShape('models/forest/Vegetation_06/Vegetation_06.glb')
  vegetation_06.addComponentOrReplace(gltfShape_25)
  const transform_31 = new Transform({
    position: new Vector3(27, 0, 10.5),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  vegetation_06.addComponentOrReplace(transform_31)
  engine.addEntity(vegetation_06)
  
  const vegetation_07 = new Entity()
  vegetation_07.setParent(forestScene)
  const gltfShape_26 = new GLTFShape('models/forest/Vegetation_07/Vegetation_07.glb')
  vegetation_07.addComponentOrReplace(gltfShape_26)
  const transform_32 = new Transform({
    position: new Vector3(27.5, 0, 11),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  vegetation_07.addComponentOrReplace(transform_32)
  engine.addEntity(vegetation_07)
  
  const vegetation_09 = new Entity()
  vegetation_09.setParent(forestScene)
  const gltfShape_27 = new GLTFShape('models/forest/Vegetation_09/Vegetation_09.glb')
  vegetation_09.addComponentOrReplace(gltfShape_27)
  const transform_33 = new Transform({
    position: new Vector3(6, 0, 6.5),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(4.752776749732568, 4.752776749732568, 4.752776749732568)
  })
  vegetation_09.addComponentOrReplace(transform_33)
  engine.addEntity(vegetation_09)
  
  const tree_Forest_Blue_01 = new Entity()
  tree_Forest_Blue_01.setParent(forestScene)
  const gltfShape_28 = new GLTFShape('models/forest/Tree_Forest_Blue_01/Tree_Forest_Blue_01.glb')
  tree_Forest_Blue_01.addComponentOrReplace(gltfShape_28)
  const transform_34 = new Transform({
    position: new Vector3(22, 0, 40),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(-0.46592325650541255, 0.6405770280317462, 0.5750615247892696)
  })
  tree_Forest_Blue_01.addComponentOrReplace(transform_34)
  engine.addEntity(tree_Forest_Blue_01)
  
  const vegetation_08 = new Entity()
  vegetation_08.setParent(forestScene)
  const gltfShape_29 = new GLTFShape('models/forest/Vegetation_08/Vegetation_08.glb')
  vegetation_08.addComponentOrReplace(gltfShape_29)
  const transform_35 = new Transform({
    position: new Vector3(6, 0, 12.5),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 3, 1)
  })
  vegetation_08.addComponentOrReplace(transform_35)
  engine.addEntity(vegetation_08)
  
  const vegetation_08_2 = new Entity()
  vegetation_08_2.setParent(forestScene)
  vegetation_08_2.addComponentOrReplace(gltfShape_29)
  const transform_36 = new Transform({
    position: new Vector3(8, 0, 13),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 3, 1)
  })
  vegetation_08_2.addComponentOrReplace(transform_36)
  engine.addEntity(vegetation_08_2)
  
  const tree_Forest_Pink_02 = new Entity()
  tree_Forest_Pink_02.setParent(forestScene)
  const gltfShape_30 = new GLTFShape('models/forest/Tree_Forest_Pink_02/Tree_Forest_Pink_02.glb')
  tree_Forest_Pink_02.addComponentOrReplace(gltfShape_30)
  const transform_37 = new Transform({
    position: new Vector3(23.5, 0, 44),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(0.8128036486689183, 1.5, 1)
  })
  tree_Forest_Pink_02.addComponentOrReplace(transform_37)
  engine.addEntity(tree_Forest_Pink_02)
  
  const tree_Leafs_02_2 = new Entity()
  tree_Leafs_02_2.setParent(forestScene)
  tree_Leafs_02_2.addComponentOrReplace(gltfShape_22)
  const transform_38 = new Transform({
    position: new Vector3(8, 0, 44),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(0.5362541212773264, 0.30537639609527023, 1)
  })
  tree_Leafs_02_2.addComponentOrReplace(transform_38)
  engine.addEntity(tree_Leafs_02_2)
  
  /*
  const decentralandLogo_01 = new Entity()
  decentralandLogo_01.setParent(forestScene)
  const gltfShape_31 = new GLTFShape('models/forest/DecentralandLogo_01/DecentralandLogo_01.glb')
  decentralandLogo_01.addComponentOrReplace(gltfShape_31)
  const transform_39 = new Transform({
    position: new Vector3(13.762607748265186, 0.5, 23.795444789891633),
    rotation: new Quaternion(0.06930858459954573, -0.06930858459954574, -0.7037018687631912, 0.7037018687631913),
    scale: new Vector3(1, 1, 1)
  })
  decentralandLogo_01.addComponentOrReplace(transform_39)
  engine.addEntity(decentralandLogo_01)
  */
 
  const vegetation_08_3 = new Entity()
  vegetation_08_3.setParent(forestScene)
  vegetation_08_3.addComponentOrReplace(gltfShape_29)
  const transform_40 = new Transform({
    position: new Vector3(27.5, 0, 23.5),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  vegetation_08_3.addComponentOrReplace(transform_40)
  engine.addEntity(vegetation_08_3)
  
  const vegetation_08_4 = new Entity()
  vegetation_08_4.setParent(forestScene)
  vegetation_08_4.addComponentOrReplace(gltfShape_29)
  const transform_41 = new Transform({
    position: new Vector3(27.5, 0, 24.5),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 3, 1)
  })
  vegetation_08_4.addComponentOrReplace(transform_41)
  engine.addEntity(vegetation_08_4)
  
  const vegetation_08_5 = new Entity()
  vegetation_08_5.setParent(forestScene)
  vegetation_08_5.addComponentOrReplace(gltfShape_29)
  const transform_42 = new Transform({
    position: new Vector3(27, 0, 24.5),
    rotation: new Quaternion(0, -0.7730104533627373, 0, 0.6343932841636457),
    scale: new Vector3(1, 3, 1)
  })
  vegetation_08_5.addComponentOrReplace(transform_42)
  engine.addEntity(vegetation_08_5)
  
  const vegetation_07_2 = new Entity()
  vegetation_07_2.setParent(forestScene)
  vegetation_07_2.addComponentOrReplace(gltfShape_26)
  const transform_43 = new Transform({
    position: new Vector3(26.5, 0, 38),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  vegetation_07_2.addComponentOrReplace(transform_43)
  engine.addEntity(vegetation_07_2)
  
  const mushrooms_04_2 = new Entity()
  mushrooms_04_2.setParent(forestScene)
  mushrooms_04_2.addComponentOrReplace(gltfShape_19)
  const transform_44 = new Transform({
    position: new Vector3(3, 0, 29),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(3.8624870579266606, 2.764381423552953, 1)
  })
  mushrooms_04_2.addComponentOrReplace(transform_44)
  engine.addEntity(mushrooms_04_2)
  
  const mushrooms_04_3 = new Entity()
  mushrooms_04_3.setParent(forestScene)
  mushrooms_04_3.addComponentOrReplace(gltfShape_19)
  const transform_45 = new Transform({
    position: new Vector3(3, 0, 29.5),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(3.8624870579266606, 3.80395007483118, 1)
  })
  mushrooms_04_3.addComponentOrReplace(transform_45)
  engine.addEntity(mushrooms_04_3)
  
  const mushrooms_04_4 = new Entity()
  mushrooms_04_4.setParent(forestScene)
  mushrooms_04_4.addComponentOrReplace(gltfShape_19)
  const transform_46 = new Transform({
    position: new Vector3(4, 0, 28),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(3.8624870579266606, 3.80395007483118, 1)
  })
  mushrooms_04_4.addComponentOrReplace(transform_46)
  engine.addEntity(mushrooms_04_4)
  
  const tree_Forest_Blue_02 = new Entity()
  tree_Forest_Blue_02.setParent(forestScene)
  const gltfShape_32 = new GLTFShape('models/forest/Tree_Forest_Blue_02/Tree_Forest_Blue_02.glb')
  tree_Forest_Blue_02.addComponentOrReplace(gltfShape_32)
  const transform_47 = new Transform({
    position: new Vector3(27, 0, 25.5),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(0.6189767602929592, 0.8978163008288291, -0.5530019177621277)
  })
  tree_Forest_Blue_02.addComponentOrReplace(transform_47)
  engine.addEntity(tree_Forest_Blue_02)
  
  const tree_03 = new Entity()
  tree_03.setParent(forestScene)
  const gltfShape_33 = new GLTFShape('models/forest/Tree_03/Tree_03.glb')
  tree_03.addComponentOrReplace(gltfShape_33)
  const transform_48 = new Transform({
    position: new Vector3(2.5, 0.5, 21.5),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(0.8135864187892956, 1.8013519198839596, 1)
  })
  tree_03.addComponentOrReplace(transform_48)
  engine.addEntity(tree_03)
  
  const bush_Fantasy_02_2 = new Entity()
  bush_Fantasy_02_2.setParent(forestScene)
  bush_Fantasy_02_2.addComponentOrReplace(gltfShape_17)
  const transform_49 = new Transform({
    position: new Vector3(24.5, 0, 2.5),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1.832176327771048, 1)
  })
  bush_Fantasy_02_2.addComponentOrReplace(transform_49)
  engine.addEntity(bush_Fantasy_02_2)
  
  const bush_Fantasy_01_2 = new Entity()
  bush_Fantasy_01_2.setParent(forestScene)
  bush_Fantasy_01_2.addComponentOrReplace(gltfShape_14)
  const transform_50 = new Transform({
    position: new Vector3(10, 0, 38.5),
    rotation: new Quaternion(0, 0.9807852804032304, 0, 0.19509032201612833),
    scale: new Vector3(1.5773502691896257, 1.5773502691896257, 1.5773502691896257)
  })
  bush_Fantasy_01_2.addComponentOrReplace(transform_50)
  engine.addEntity(bush_Fantasy_01_2)
  
  const mushrooms_04_5 = new Entity()
  mushrooms_04_5.setParent(forestScene)
  mushrooms_04_5.addComponentOrReplace(gltfShape_19)
  const transform_51 = new Transform({
    position: new Vector3(27, 0, 14),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(3.8624870579266606, 5.73442276544257, 3.230402407389267)
  })
  mushrooms_04_5.addComponentOrReplace(transform_51)
  engine.addEntity(mushrooms_04_5)
  
  const plant_02 = new Entity()
  plant_02.setParent(forestScene)
  const gltfShape_34 = new GLTFShape('models/forest/Plant_02/Plant_02.glb')
  plant_02.addComponentOrReplace(gltfShape_34)
  const transform_52 = new Transform({
    position: new Vector3(27, 0, 36),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  plant_02.addComponentOrReplace(transform_52)
  engine.addEntity(plant_02)
  
  const plant_04 = new Entity()
  plant_04.setParent(forestScene)
  const gltfShape_35 = new GLTFShape('models/forest/Plant_04/Plant_04.glb')
  plant_04.addComponentOrReplace(gltfShape_35)
  const transform_53 = new Transform({
    position: new Vector3(25, 0, 9),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  plant_04.addComponentOrReplace(transform_53)
  engine.addEntity(plant_04)
  
  const bush_Fantasy_03_2 = new Entity()
  bush_Fantasy_03_2.setParent(forestScene)
  bush_Fantasy_03_2.addComponentOrReplace(gltfShape_16)
  const transform_54 = new Transform({
    position: new Vector3(6, 0, 5.5),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  bush_Fantasy_03_2.addComponentOrReplace(transform_54)
  engine.addEntity(bush_Fantasy_03_2)
  
  const mushrooms_04_6 = new Entity()
  mushrooms_04_6.setParent(forestScene)
  mushrooms_04_6.addComponentOrReplace(gltfShape_19)
  const transform_55 = new Transform({
    position: new Vector3(27.5, 0, 14.5),
    rotation: new Quaternion(0, -0.38268343236508984, 0, 0.9238795325112868),
    scale: new Vector3(3.8624870579266606, 3.80395007483118, 1)
  })
  mushrooms_04_6.addComponentOrReplace(transform_55)
  engine.addEntity(mushrooms_04_6)
  
  const plant_01_2 = new Entity()
  plant_01_2.setParent(forestScene)
  plant_01_2.addComponentOrReplace(gltfShape_13)
  const transform_56 = new Transform({
    position: new Vector3(3.5, 0, 30),
    rotation: new Quaternion(0, -0.38268343236508984, 0, 0.9238795325112868),
    scale: new Vector3(1, 4.5, 2.250826972140313)
  })
  plant_01_2.addComponentOrReplace(transform_56)
  engine.addEntity(plant_01_2)
  
  const tree_Forest_Green_01 = new Entity()
  tree_Forest_Green_01.setParent(forestScene)
  const gltfShape_36 = new GLTFShape('models/forest/Tree_Forest_Green_01/Tree_Forest_Green_01.glb')
  tree_Forest_Green_01.addComponentOrReplace(gltfShape_36)
  const transform_57 = new Transform({
    position: new Vector3(27.5, 0, 34.5),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  tree_Forest_Green_01.addComponentOrReplace(transform_57)
  engine.addEntity(tree_Forest_Green_01)
  
  const tree_Forest_Green_02 = new Entity()
  tree_Forest_Green_02.setParent(forestScene)
  const gltfShape_37 = new GLTFShape('models/forest/Tree_Forest_Green_02/Tree_Forest_Green_02.glb')
  tree_Forest_Green_02.addComponentOrReplace(gltfShape_37)
  const transform_58 = new Transform({
    position: new Vector3(4, 0, 9.5),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(0.4437065446973385, 0.4818243271628493, 0.33506084445198425)
  })
  tree_Forest_Green_02.addComponentOrReplace(transform_58)
  engine.addEntity(tree_Forest_Green_02)
  
  /*
  const launchTubeGlass = new Entity()
  launchTubeGlass.setParent(forestScene)
  const gltfShape_38 = new GLTFShape('models/forest/launchTubeGlass.glb')
  launchTubeGlass.addComponentOrReplace(gltfShape_38)
  const transform_59 = new Transform({
    position: new Vector3(16.5, 0, 3),
    rotation: new Quaternion(0, -0.9951847266721972, 0, -0.09801714032956067),
    scale: new Vector3(1, 1, 1)
  })
  launchTubeGlass.addComponentOrReplace(transform_59)
  engine.addEntity(launchTubeGlass)
  
  const launchTubeGlass_2 = new Entity()
  launchTubeGlass_2.setParent(forestScene)
  launchTubeGlass_2.addComponentOrReplace(gltfShape_38)
  const transform_60 = new Transform({
    position: new Vector3(13, 0, 40.5),
    rotation: new Quaternion(0, -0.3826834323650898, 0, 0.9238795325112867),
    scale: new Vector3(1, 1, 1)
  })
  launchTubeGlass_2.addComponentOrReplace(transform_60)
  engine.addEntity(launchTubeGlass_2)
  
  const launchTubeGlass_3 = new Entity()
  launchTubeGlass_3.setParent(forestScene)
  launchTubeGlass_3.addComponentOrReplace(gltfShape_38)
  const transform_61 = new Transform({
    position: new Vector3(25, 0, 40),
    rotation: new Quaternion(0, 1.5265566588595902e-16, 0, 1.0000000000000004),
    scale: new Vector3(1, 1, 1)
  })
  launchTubeGlass_3.addComponentOrReplace(transform_61)
  engine.addEntity(launchTubeGlass_3)
  
  const launchTubeGlass_4 = new Entity()
  launchTubeGlass_4.setParent(forestScene)
  launchTubeGlass_4.addComponentOrReplace(gltfShape_38)
  const transform_62 = new Transform({
    position: new Vector3(14.5, 0.5, 24),
    rotation: new Quaternion(0, -0.3826834323650898, 0, 0.9238795325112867),
    scale: new Vector3(1, 1, 1)
  })
  launchTubeGlass_4.addComponentOrReplace(transform_62)
  engine.addEntity(launchTubeGlass_4)
  
  const launchTubeGlass_5 = new Entity()
  launchTubeGlass_5.setParent(forestScene)
  launchTubeGlass_5.addComponentOrReplace(gltfShape_38)
  const transform_63 = new Transform({
    position: new Vector3(6.5, 0, 30.5),
    rotation: new Quaternion(0, -0.6343932841636457, 0, 0.7730104533627371),
    scale: new Vector3(1, 1, 1)
  })
  launchTubeGlass_5.addComponentOrReplace(transform_63)
  engine.addEntity(launchTubeGlass_5)
  
  const launchTubeGlass_6 = new Entity()
  launchTubeGlass_6.setParent(forestScene)
  launchTubeGlass_6.addComponentOrReplace(gltfShape_38)d
  const transform_64 = new Transform({
    position: new Vector3(23.5, 0, 17),
    rotation: new Quaternion(0, 0.9238795325112866, 0, 0.38268343236508995),
    scale: new Vector3(1, 1, 1)
  })
  launchTubeGlass_6.addComponentOrReplace(transform_64)
  engine.addEntity(launchTubeGlass_6)
  */

  const crater_01 = new Entity()
  crater_01.setParent(forestScene)
  const gltfShape_39 = new GLTFShape('models/forest/Crater_01/Crater_01.glb')
  crater_01.addComponentOrReplace(gltfShape_39)
  const transform_65 = new Transform({
    position: new Vector3(14.5, 0, 24),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(2.995550908522951, 1, 3.473884675773718)
  })
  crater_01.addComponentOrReplace(transform_65)
  engine.addEntity(crater_01)
  
  const planet_02 = new Entity()
  planet_02.setParent(forestScene)
  const gltfShape_40 = new GLTFShape('models/forest/Planet_02/Planet_02.glb')
  planet_02.addComponentOrReplace(gltfShape_40)
  const transform_66 = new Transform({
    position: new Vector3(24, 16, 19.5),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(0.759394701758513, 0.8106440009950546, 0.8374206701840841)
  })
  planet_02.addComponentOrReplace(transform_66)
  engine.addEntity(planet_02)
  
  const planet_05 = new Entity()
  planet_05.setParent(forestScene)
  const gltfShape_41 = new GLTFShape('models/forest/Planet_05/Planet_05.glb')
  planet_05.addComponentOrReplace(gltfShape_41)
  const transform_67 = new Transform({
    position: new Vector3(16, 18, 27.5),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  planet_05.addComponentOrReplace(transform_67)
  engine.addEntity(planet_05)
  
  const plantSF_01 = new Entity()
  plantSF_01.setParent(forestScene)
  const gltfShape_42 = new GLTFShape('models/forest/PlantSF_01/PlantSF_01.glb')
  plantSF_01.addComponentOrReplace(gltfShape_42)
  const transform_68 = new Transform({
    position: new Vector3(16, 0, 14.5),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  plantSF_01.addComponentOrReplace(transform_68)
  engine.addEntity(plantSF_01)
  
  const plantSF_09 = new Entity()
  plantSF_09.setParent(forestScene)
  const gltfShape_43 = new GLTFShape('models/forest/PlantSF_09/PlantSF_09.glb')
  plantSF_09.addComponentOrReplace(gltfShape_43)
  const transform_69 = new Transform({
    position: new Vector3(16, 0, 14),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  plantSF_09.addComponentOrReplace(transform_69)
  engine.addEntity(plantSF_09)
  
  const plantSF_07 = new Entity()
  plantSF_07.setParent(forestScene)
  const gltfShape_44 = new GLTFShape('models/forest/PlantSF_07/PlantSF_07.glb')
  plantSF_07.addComponentOrReplace(gltfShape_44)
  const transform_70 = new Transform({
    position: new Vector3(16.5, 0, 13.5),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(2.309321036434145, 2.3370403037427927, 2.0429834465789725)
  })
  plantSF_07.addComponentOrReplace(transform_70)
  engine.addEntity(plantSF_07)
  
  const plantSF_11 = new Entity()
  plantSF_11.setParent(forestScene)
  const gltfShape_45 = new GLTFShape('models/forest/PlantSF_11/PlantSF_11.glb')
  plantSF_11.addComponentOrReplace(gltfShape_45)
  const transform_71 = new Transform({
    position: new Vector3(14, 0.5, 4),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1.4325920925981013, 1)
  })
  plantSF_11.addComponentOrReplace(transform_71)
  engine.addEntity(plantSF_11)
  
  const plantSF_06 = new Entity()
  plantSF_06.setParent(forestScene)
  const gltfShape_46 = new GLTFShape('models/forest/PlantSF_06/PlantSF_06.glb')
  plantSF_06.addComponentOrReplace(gltfShape_46)
  const transform_72 = new Transform({
    position: new Vector3(14.5, 0, 2),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 2.046032810074688, 1)
  })
  plantSF_06.addComponentOrReplace(transform_72)
  engine.addEntity(plantSF_06)
  
  const dirt_04 = new Entity()
  dirt_04.setParent(forestScene)
  const gltfShape_47 = new GLTFShape('models/forest/Dirt_04/Dirt_04.glb')
  dirt_04.addComponentOrReplace(gltfShape_47)
  const transform_73 = new Transform({
    position: new Vector3(19, 0, 42.5),
    rotation: new Quaternion(0, 0.3826834323650897, 0, 0.9238795325112867),
    scale: new Vector3(5.785707175601651, 2.5, 1.5204051016748537)
  })
  dirt_04.addComponentOrReplace(transform_73)
  engine.addEntity(dirt_04)
  
  const floor_Module_06 = new Entity()
  floor_Module_06.setParent(forestScene)
  const gltfShape_48 = new GLTFShape('models/forest/Floor_Module_06/Floor_Module_06.glb')
  floor_Module_06.addComponentOrReplace(gltfShape_48)
  const transform_74 = new Transform({
    position: new Vector3(16, 0, 24),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  floor_Module_06.addComponentOrReplace(transform_74)
  engine.addEntity(floor_Module_06)
  
  
  
  const dirt_03 = new Entity()
  dirt_03.setParent(forestScene)
  const gltfShape_50 = new GLTFShape('models/forest/Dirt_03/Dirt_03.glb')
  dirt_03.addComponentOrReplace(gltfShape_50)
  const transform_82 = new Transform({
    position: new Vector3(5, 0, 5.5),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(4, 1, 3.5)
  })
  dirt_03.addComponentOrReplace(transform_82)
  engine.addEntity(dirt_03)
  
  const dirt_02 = new Entity()
  dirt_02.setParent(forestScene)
  const gltfShape_51 = new GLTFShape('models/forest/Dirt_02/Dirt_02.glb')
  dirt_02.addComponentOrReplace(gltfShape_51)
  const transform_83 = new Transform({
    position: new Vector3(28, 0, 24.5),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1.2886751345948135, 1.2886751345948135, 1.2886751345948135)
  })
  dirt_02.addComponentOrReplace(transform_83)
  engine.addEntity(dirt_02)
  
  const crater_01_2 = new Entity()
  crater_01_2.setParent(forestScene)
  crater_01_2.addComponentOrReplace(gltfShape_39)
  const transform_84 = new Transform({
    position: new Vector3(13, 0, 40.5),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1.5766556550215682, 0.6499510549704475, 1.8585532568132095)
  })
  crater_01_2.addComponentOrReplace(transform_84)
  engine.addEntity(crater_01_2)
  
  const plantSF_04 = new Entity()
  plantSF_04.setParent(forestScene)
  const gltfShape_52 = new GLTFShape('models/forest/PlantSF_04/PlantSF_04.glb')
  plantSF_04.addComponentOrReplace(gltfShape_52)
  const transform_85 = new Transform({
    position: new Vector3(16, 0, 27.5),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, 1, 1)
  })
  plantSF_04.addComponentOrReplace(transform_85)
  engine.addEntity(plantSF_04)
  
  const dirt_04_2 = new Entity()
  dirt_04_2.setParent(forestScene)
  dirt_04_2.addComponentOrReplace(gltfShape_47)
  const transform_86 = new Transform({
    position: new Vector3(16.5, 0, 3),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(2.334212554941274, 1, 2.1193957928245766)
  })
  dirt_04_2.addComponentOrReplace(transform_86)
  engine.addEntity(dirt_04_2)
  
  const crater_01_3 = new Entity()
  crater_01_3.setParent(forestScene)
  crater_01_3.addComponentOrReplace(gltfShape_39)
  const transform_87 = new Transform({
    position: new Vector3(23.5, 0, 17),
    rotation: new Quaternion(0, 0, 0, 1),
    scale: new Vector3(1, -1.999, 1)
  })
  crater_01_3.addComponentOrReplace(transform_87)
  engine.addEntity(crater_01_3)
  
  const crater_01_4 = new Entity()
  crater_01_4.setParent(forestScene)
  crater_01_4.addComponentOrReplace(gltfShape_39)
  const transform_88 = new Transform({
    position: new Vector3(6.5, 0, 30.5),
    rotation: new Quaternion(0, -0.38268343236508984, 0, 0.9238795325112868),
    scale: new Vector3(2.4037557532732468, 0.6499510549704475, 2.217566886693554)
  })
  crater_01_4.addComponentOrReplace(transform_88)
  engine.addEntity(crater_01_4)
}