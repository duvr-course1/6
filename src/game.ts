import utils from "../node_modules/decentraland-ecs-utils/index"
import {spawnLaunchPad, spawnLaunchTube, spawnBoxX, spawnCone, spawnCylinder, spawnConeX, spawnCylinderX, spawnGltfX} from "./modules/SpawnerFunctionsGame1"
// import {createForest} from "./modules/forest"

// This scene entity wraps the whole scene, so it can be moved, rotated, or sized as may be needed
const scene = new Entity()
const sceneTransform = new Transform({
   position: new Vector3(0, 0, 0),
   rotation: Quaternion.Euler(0, 0, 0),
   scale: new Vector3(1, 1, 1) })
scene.addComponent(sceneTransform)
engine.addEntity(scene)

// ground
let floor = new Entity()
floor.addComponent(new GLTFShape("materials/FloorBaseGrass.glb"))
floor.addComponent(new Transform({
  position: new Vector3(8, 0, 8),
  scale:new Vector3(1.6, 0.1, 1.6)
  }))
engine.addEntity(floor)

//createForest(scene)

// BUILD SCENE
// Positions:
let White2X = 10
let White2Z = 2

let Red2X = 11
let Red2Z = 6

let WhiteX = 11
let WhiteZ = 10

let BlueX = 10
let BlueZ = 14

let padY = 0
let tubeY = 0
let heightBasic = 10

// BUILDER PLATFORM (HTC Base) 
const launchpadWhite2 = spawnLaunchPad(new GLTFShape('models/HTC_Base/HTC_Base.glb'), White2X, padY, White2Z)
const launchtubeWhite2 = spawnLaunchTube(new GLTFShape('models/LaunchTubeGlassWhite.glb'), White2X, tubeY, White2Z, 135, 1)
launchtubeWhite2.addComponent( new OnClick(() => {
  let Height = heightBasic
  let SpeedUp = 0.6
  let SpeedDown = 2
  launchpadWhite2.addComponentOrReplace(new utils.MoveTransformComponent(new Vector3(White2X,padY,White2Z), new Vector3(White2X,Height,White2Z), SpeedUp))
  launchpadWhite2.addComponentOrReplace(new utils.Delay(SpeedUp*1000, () => {
    launchpadWhite2.addComponentOrReplace(new utils.MoveTransformComponent(new Vector3(White2X,Height,White2Z), new Vector3(White2X,padY,White2Z), SpeedDown))
    }))
})
)

const launchpadRed2 = spawnLaunchPad(new GLTFShape('models/HTC_Base/HTC_Base.glb'), Red2X, padY, Red2Z)
const launchtubeRed2 = spawnLaunchTube(new GLTFShape('models/LaunchTubeGlassRed2.glb'), Red2X, tubeY, Red2Z, 90, 1.2)
launchtubeRed2.addComponent(new AudioSource(new AudioClip('sounds/415205__yin-yang-jake007__lighter-flicking.mp3')))

launchtubeRed2.addComponent( new OnClick(() => {
  let Height = heightBasic*2
  let SpeedUp = 0.8
  let SpeedDown = 3
  // launchtubeRed2.getComponent(AudioSource).volume = 1
  //launchtubeRed2.getComponent(AudioSource).playOnce()
  launchpadRed2.addComponentOrReplace(new utils.MoveTransformComponent(new Vector3(Red2X,padY+0.2,Red2Z), new Vector3(Red2X,Height,Red2Z), SpeedUp))
  launchpadRed2.addComponentOrReplace(new utils.Delay(SpeedUp*1000, () => {
    launchpadRed2.addComponentOrReplace(new utils.MoveTransformComponent(new Vector3(Red2X,Height,Red2Z), new Vector3(Red2X,padY+0.2,Red2Z), SpeedDown))
    }))
})
)


const launchpadWhite = spawnLaunchPad(new GLTFShape('models/HTC_Base/HTC_Base.glb'), WhiteX, padY, WhiteZ)
const launchtubeWhite = spawnLaunchTube(new GLTFShape('models/LaunchTubeGlassWhite.glb'), WhiteX, tubeY, WhiteZ, 90, 1)
launchtubeWhite.addComponent( new OnClick(() => {
  let Height = heightBasic
  let SpeedUp = 0.5
  let SpeedDown = 2
  launchpadWhite.addComponentOrReplace(new utils.MoveTransformComponent(new Vector3(WhiteX,padY,WhiteZ), new Vector3(WhiteX,Height,WhiteZ), SpeedUp))
  launchpadWhite.addComponentOrReplace(new utils.Delay(SpeedUp*1000, () => {
    launchpadWhite.addComponentOrReplace(new utils.MoveTransformComponent(new Vector3(WhiteX,Height,WhiteZ), new Vector3(WhiteX,padY,WhiteZ), SpeedDown))
    }))
})
)


const launchpadBlue = spawnLaunchPad(new GLTFShape('models/HTC_Base/HTC_Base.glb'), BlueX, padY, BlueZ)
const launchtubeBlue = spawnLaunchTube(new GLTFShape('models/LaunchTubeGlassBlue.glb'), BlueX, tubeY, BlueZ, 45, 1.5)
launchtubeBlue.addComponent( new OnClick(() => {
  let Height = heightBasic*1.5
  let SpeedUp = 0.5
  let SpeedDown = 2.5
  launchpadBlue.addComponentOrReplace(new utils.MoveTransformComponent(new Vector3(BlueX,padY,BlueZ), new Vector3(BlueX,Height,BlueZ), SpeedUp))
  launchpadBlue.addComponentOrReplace(new utils.Delay(SpeedUp*1000, () => {
    launchpadBlue.addComponentOrReplace(new utils.MoveTransformComponent(new Vector3(BlueX,Height,BlueZ), new Vector3(BlueX,padY,BlueZ), SpeedDown))
    }))
})
)

// Red2 SOUNDS 
// PROXIMITY CHECK (from basic-interactions-master example)
export class Proximity {
  update() {
    let transform = launchpadRed2.getComponent(Transform)
    let dist = distance(transform.position, camera.position)
    if ( dist < 5) {
      launchtubeRed2.getComponent(AudioSource).volume = 1-dist*0.1
        launchtubeRed2.getComponent(AudioSource).playOnce()
      }
    }
  }

  engine.addSystem(new Proximity()) 


// Object that tracks user position and rotation

const camera = Camera.instance


// Get distance
/*ss
Note:
This function really returns distance squared, as it's a lot more efficient to calculate.
The square root operation is expensive and isn't really necessary if we compare the result to squared values.
We also use {x,z} not {x,y}. The y-coordinate is how high up it is.
*/
function distance(pos1: Vector3, pos2: Vector3): number {
  const a = pos1.x - pos2.x
  const b = pos1.z - pos2.z
  return a * a + b * b
}
// ------- PROXIMITY CHECK




// const hud:BuilderHUD =  new BuilderHUD()
// hud.setDefaultParent(scene)
// hud.attachToEntity(cube)
// hud.attachToEntity(rabbit1)
