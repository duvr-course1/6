import utils from "../node_modules/decentraland-ecs-utils/index"
import {spawnLaunchPad, spawnLaunchTube, spawnBoxX, spawnCone, spawnCylinder, spawnConeX, spawnCylinderX, spawnGltfX} from "./modules/SpawnerFunctionsGame1"
import {createForest} from "./modules/forest"

// This scene entity wraps the whole scene, so it can be moved, rotated, or sized as may be needed
const scene = new Entity()
const sceneTransform = new Transform({
   position: new Vector3(0, 0, 0),
   rotation: Quaternion.Euler(0, 0, 0),
   scale: new Vector3(1, 1, 1) })
scene.addComponent(sceneTransform)
engine.addEntity(scene)

// ground
let floor = new Entity()
floor.addComponent(new GLTFShape("materials/FloorBaseGrass.glb"))
floor.addComponent(new Transform({
  position: new Vector3(16, 0, 24),
  scale:new Vector3(3.2, 0.1, 4.8)
}))

engine.addEntity(floor)

let blueMaterial = new Material()
blueMaterial.albedoColor = Color3.Blue()
let greenMaterial = new Material()
greenMaterial.albedoColor = Color3.Green()
let whiteMaterial = new Material()
whiteMaterial.albedoColor = Color3.White()

createForest(scene)
/*const treeSycamore_02 = new Entity()
treeSycamore_02.setParent(scene)
const gltfShape_2 = new GLTFShape('models/forest/TreeSycamore_02/TreeSycamore_02.glb')
treeSycamore_02.addComponentOrReplace(gltfShape_2)
const transform_8 = new Transform({
  position: new Vector3(26, 0, 10),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
treeSycamore_02.addComponentOrReplace(transform_8)
engine.addEntity(treeSycamore_02)
*/

// BUILD SCENE
// Positions:
let WhiteX = 15
let WhiteY = 20
let RedX = 15
let RedY = 22
let Red2X = 15
let Red2Y = 24
let BlueX = 15
let BlueY = 26

let GreenX = 17
let GreenY = 20
let BlackX = 17
let BlackY = 22
let BasicX = 17
let BasicY = 24

// BUILDER PLATFORM (HTC Base) 
const launchpadWhite = spawnLaunchPad(new GLTFShape('models/HTC_Base/HTC_Base.glb'), WhiteX, 0, WhiteY)
// const launchtubeWhite = spawnLaunchTube(new GLTFShape('models/LaunchTubeGlassWhite.glb'), WhiteX, 0, WhiteY, 45, 1.1)
const launchtubeWhite = spawnLaunchTube(new GLTFShape('models/LaunchTubeGlassWhite.glb'), WhiteX, 0, WhiteY, 90, 1.1)
launchtubeWhite.addComponent( new OnClick(() => {
  let Height = 15
  let SpeedUp = 0.5
  let SpeedDown = 3
  launchpadWhite.addComponentOrReplace(new utils.MoveTransformComponent(new Vector3(WhiteX,0,WhiteY), new Vector3(WhiteX,Height,WhiteY), SpeedUp))
  launchpadWhite.addComponentOrReplace(new utils.Delay(SpeedUp*1000, () => {
    launchpadWhite.addComponentOrReplace(new utils.MoveTransformComponent(new Vector3(WhiteX,Height,WhiteY), new Vector3(WhiteX,0,WhiteY), SpeedDown))
    }))
})
)
const launchpadRed = spawnLaunchPad(new GLTFShape('models/HTC_Base/HTC_Base.glb'), RedX, 0, RedY)
// const launchtubeRed = spawnLaunchTube(new GLTFShape('models/LaunchTubeGlassRed.glb'), RedX, 0, RedY, 30, 0.8)
const launchtubeRed = spawnLaunchTube(new GLTFShape('models/LaunchTubeGlassWhite.glb'), RedX, 0, RedY, 90, 1.1)
launchtubeRed.addComponent( new OnClick(() => {
  let Height = 15
  let SpeedUp = 0.5
  let SpeedDown = 3
  launchpadRed.addComponentOrReplace(new utils.MoveTransformComponent(new Vector3(RedX,0,RedY), new Vector3(RedX,Height,RedY), SpeedUp))
  launchpadRed.addComponentOrReplace(new utils.Delay(SpeedUp*1000, () => {
    launchpadRed.addComponentOrReplace(new utils.MoveTransformComponent(new Vector3(RedX,Height,RedY), new Vector3(RedX,0,RedY), SpeedDown))
    }))
})
)

const launchpadRed2 = spawnLaunchPad(new GLTFShape('models/HTC_Base/HTC_Base.glb'), Red2X, 0, Red2Y)
// const launchtubeRed2 = spawnLaunchTube(new GLTFShape('models/LaunchTubeGlassRed2.glb'), Red2X, 0, Red2Y, -45, 0.8)
const launchtubeRed2 = spawnLaunchTube(new GLTFShape('models/LaunchTubeGlassWhite.glb'), Red2X, 0, Red2Y, 90, 1.1)
launchtubeRed2.addComponent(new AudioSource(new AudioClip('sounds/415205__yin-yang-jake007__lighter-flicking.mp3')))
// launchtubeRed2.getComponent(AudioSource).volume = 0.5
// launchtubeRed2.getComponent(AudioSource).playing = true

launchtubeRed2.addComponent( new OnClick(() => {
  let Height = 15
  let SpeedUp = 0.5
  let SpeedDown = 3
  // launchtubeRed2.getComponent(AudioSource).volume = 1
  launchtubeRed2.getComponent(AudioSource).playOnce()
  launchpadRed2.addComponentOrReplace(new utils.MoveTransformComponent(new Vector3(Red2X,0,Red2Y), new Vector3(Red2X,Height,Red2Y), SpeedUp))
  launchpadRed2.addComponentOrReplace(new utils.Delay(SpeedUp*1000, () => {
    launchpadRed2.addComponentOrReplace(new utils.MoveTransformComponent(new Vector3(Red2X,Height,Red2Y), new Vector3(Red2X,0,Red2Y), SpeedDown))
    }))
})
)

const launchpadBlue = spawnLaunchPad(new GLTFShape('models/HTC_Base/HTC_Base.glb'), BlueX, 0, BlueY)
// const launchtubeBlue = spawnLaunchTube(new GLTFShape('models/LaunchTubeGlassBlue.glb'), BlueX, 0, BlueY, 130, 1)
const launchtubeBlue = spawnLaunchTube(new GLTFShape('models/LaunchTubeGlassWhite.glb'), BlueX, 0, BlueY, 90, 1.1)
launchtubeBlue.addComponent( new OnClick(() => {
  let Height = 15
  let SpeedUp = 0.5
  let SpeedDown = 3
  launchpadBlue.addComponentOrReplace(new utils.MoveTransformComponent(new Vector3(BlueX,0,BlueY), new Vector3(BlueX,Height,BlueY), SpeedUp))
  launchpadBlue.addComponentOrReplace(new utils.Delay(SpeedUp*1000, () => {
    launchpadBlue.addComponentOrReplace(new utils.MoveTransformComponent(new Vector3(BlueX,Height,BlueY), new Vector3(BlueX,0,BlueY), SpeedDown))
    }))
})
)

const launchpadGreen = spawnLaunchPad(new GLTFShape('models/HTC_Base/HTC_Base.glb'), GreenX, 0, GreenY)
// const launchtubeGreen = spawnLaunchTube(new GLTFShape('models/LaunchTubeGlassBlack.glb'), GreenX, 0, GreenY, 0, 0.75)
const launchtubeGreen = spawnLaunchTube(new GLTFShape('models/LaunchTubeGlassWhite.glb'), GreenX, 0, GreenY, -90, 1.1)
launchtubeGreen.addComponent( new OnClick(() => {
  let Height = 15
  let SpeedUp = 0.5
  let SpeedDown = 3
  launchpadGreen.addComponentOrReplace(new utils.MoveTransformComponent(new Vector3(GreenX,0,GreenY), new Vector3(GreenX,Height,GreenY), SpeedUp))
  launchpadGreen.addComponentOrReplace(new utils.Delay(SpeedUp*1000, () => {
    launchpadGreen.addComponentOrReplace(new utils.MoveTransformComponent(new Vector3(GreenX,Height,GreenY), new Vector3(GreenX,0,GreenY), SpeedDown))
    }))
})
)

const launchpadBlack = spawnLaunchPad(new GLTFShape('models/HTC_Base/HTC_Base.glb'), BlackX, 0, BlackY)
// const launchtubeBlack = spawnLaunchTube(new GLTFShape('models/LaunchTubeGlassBlack.glb'), BlackX, 0, BlackY, 230, 0.8)
const launchtubeBlack = spawnLaunchTube(new GLTFShape('models/LaunchTubeGlassWhite.glb'), BlackX, 0, BlackY, -90, 1.1)
launchtubeBlack.addComponent( new OnClick(() => {
  let Height = 15
  let SpeedUp = 0.5
  let SpeedDown = 3
  launchpadBlack.addComponentOrReplace(new utils.MoveTransformComponent(new Vector3(BlackX,0,BlackY), new Vector3(BlackX,Height,BlackY), SpeedUp))
  launchpadBlack.addComponentOrReplace(new utils.Delay(SpeedUp*1000, () => {
    launchpadBlack.addComponentOrReplace(new utils.MoveTransformComponent(new Vector3(BlackX,Height,BlackY), new Vector3(BlackX,0,BlackY), SpeedDown))
    }))
})
)

const launchpadBasic = spawnLaunchPad(new GLTFShape('models/HTC_Base/HTC_Base.glb'), BasicX, 0, BasicY)
// const launchtubeBasic= spawnLaunchTube(new GLTFShape('models/LaunchTubeGlass.glb'), BasicX, 0, BasicY, 70, 1.4)
const launchtubeBasic= spawnLaunchTube(new GLTFShape('models/LaunchTubeGlassWhite.glb'), BasicX, 0, BasicY, -90, 1.1)
launchtubeBasic.addComponent( new OnClick(() => {
  let Height = 15
  let SpeedUp = 0.5
  let SpeedDown = 3
  launchpadBasic.addComponentOrReplace(new utils.MoveTransformComponent(new Vector3(BasicX,0,BasicY), new Vector3(BasicX,Height,BasicY), SpeedUp))
  launchpadBasic.addComponentOrReplace(new utils.Delay(SpeedUp*1000, () => {
    launchpadBasic.addComponentOrReplace(new utils.MoveTransformComponent(new Vector3(BasicX,Height,BasicY), new Vector3(BasicX,0,BasicY), SpeedDown))
    }))
})
)

// SOUNDS 


// PROXIMITY CHECK (from basic-interactions-master example)
export class Proximity {
  update() {
    let transform = launchpadRed2.getComponent(Transform)
    let dist = distance(transform.position, camera.position)
    if ( dist < 5) {
      launchtubeRed2.getComponent(AudioSource).volume = 1-dist*0.1
        launchtubeRed2.getComponent(AudioSource).playOnce()
      }
    }
  }

  engine.addSystem(new Proximity())

// Object that tracks user position and rotation

const camera = Camera.instance

// Get distance
/*ss
Note:
This function really returns distance squared, as it's a lot more efficient to calculate.
The square root operation is expensive and isn't really necessary if we compare the result to squared values.
We also use {x,z} not {x,y}. The y-coordinate is how high up it is.
*/
function distance(pos1: Vector3, pos2: Vector3): number {
  const a = pos1.x - pos2.x
  const b = pos1.z - pos2.z
  return a * a + b * b
}
// ------- PROXIMITY CHECK


/* OLD
const launchTubeGlass = new Entity()
launchTubeGlass.setParent(scene)
const gltfShape_5 = new GLTFShape('models/launchTubeGlass.glb')
launchTubeGlass.addComponentOrReplace(gltfShape_5)
const transform_7 = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
launchTubeGlass.addComponentOrReplace(transform_7)
engine.addEntity(launchTubeGlass)

const launchpadButtonBase1 = new Entity()
launchpadButtonBase1.setParent(scene)
const gltfShape_3 = new GLTFShape('models/LaunchpadButtonBase1.glb')
launchpadButtonBase1.addComponentOrReplace(gltfShape_3)
const transform_4 = new Transform({
  position: new Vector3(6, 0, 10),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1.75, 1.25, 1)
})
launchpadButtonBase1.addComponentOrReplace(transform_4)
engine.addEntity(launchpadButtonBase1)

// -------- PLATFORM TESTING
const platform1 = new Entity()
platform1.setParent(scene)
const platform1GltfShape = new GLTFShape('models/HTC_Base/HTC_Base.glb')
platform1.addComponentOrReplace(platform1GltfShape)
const platform1Transform = new Transform({
  position: new Vector3(8, 0.0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(0.45, 0.04, 0.45)
})
platform1.addComponentOrReplace(platform1Transform) 
engine.addEntity(platform1)

// BLUE platform
// const platform1 = spawnBoxX( 8,0,8, 0,0,0, 1.5,0.1,1.5)
// platform1.addComponentOrReplace(blueMaterial)

// TEST platform
const platformTest = spawnBoxX( 4,0,8, 0,0,0, 1.5,0.1,1.5)
//const platformTest = spawnCylinderX( 4,0,8, 0,0,0, 1,0.1,1)
platformTest.addComponentOrReplace(greenMaterial)

platformTest.addComponent( new OnClick(() => {
  let SpeedUp = 0.5
  platformTest.addComponentOrReplace(new utils.MoveTransformComponent(new Vector3(4,0,8), new Vector3(4,10,8), SpeedUp))
  platformTest.addComponentOrReplace(new utils.Delay(SpeedUp*1000, () => {
      platformTest.addComponentOrReplace(new utils.MoveTransformComponent(new Vector3(4,10,8), new Vector3(4,0,8), 5))
    }))
  })
)
// -----------------------------

launchTubeGlass.addComponent( new OnClick(() => {
  let StartPos = new Vector3(8,0,8)
  let StartPosRing = new Vector3(8,0.02,8)
  let EndPos = new Vector3(8,10,8)
  let SpeedUp = 0.5
  let SpeedDown = 6
  platformTest.addComponentOrReplace(new utils.MoveTransformComponent(new Vector3(4,0,8), new Vector3(4,10,8), SpeedUp))
  platformTest.addComponentOrReplace(new utils.Delay(SpeedUp*1000, () => {
    platformTest.addComponentOrReplace(new utils.MoveTransformComponent(new Vector3(4,10,8), new Vector3(4,0,8), SpeedDown/2))
    }))
  platform1.addComponentOrReplace(new utils.MoveTransformComponent(StartPos, EndPos, SpeedUp))
  platform1.addComponentOrReplace(new utils.Delay(SpeedUp*1000, () => {
    platform1.addComponentOrReplace(new utils.MoveTransformComponent(EndPos, StartPos, SpeedDown))
  }))
  //platform1Ring.addComponentOrReplace(new utils.MoveTransformComponent(StartPosRing, EndPos, SpeedUp))
  //platform1Ring.addComponentOrReplace(new utils.Delay(SpeedUp*1000, () => {
  //  platform1Ring.addComponentOrReplace(new utils.MoveTransformComponent(EndPos, StartPosRing, SpeedDown))
  //}))
})
)

//Ring
const platform1Ring = new Entity()
platform1Ring.setParent(scene)
const platform1RingGltfShape = new GLTFShape('models/platformRing.glb')
platform1Ring.addComponentOrReplace(platform1RingGltfShape)
const platform1RingTransform = new Transform({
  position: new Vector3(8, 0.02, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(0.75, 1, 0.75)
})
platform1Ring.addComponentOrReplace(platform1RingTransform)
engine.addEntity(platform1Ring)
// Ring end
*/

// platformTest.getComponent(BoxShape).visible = false
// platform1.getComponent(BoxShape).visible = false


/*
const cubeTrigger = new Entity()
engine.addEntity(cubeTrigger)
cubeTrigger.addComponent(new Transform({
  position: new Vector3(8,0,8)
}))
*/

// TRIGGER COMPONENT - nefunguje
/*
cube.addComponent(new utils.TriggerComponent(
  new utils.TriggerBoxShape(Vector3.One(), Vector3.Zero()), //shape
  0, //layer
  0, //triggeredByLayer
  () => { //onCameraEnter
    log("triggered!")
    let StartPos = new Vector3(8,0,8)
    let EndPos = new Vector3(8,10,8)
    let SpeedUp = 0.5
    cube.addComponentOrReplace(new utils.MoveTransformComponent(StartPos, EndPos, SpeedUp))
    cube.addComponentOrReplace(new utils.Delay(SpeedUp*1000, () => {
      cube.addComponentOrReplace(new utils.MoveTransformComponent(EndPos, StartPos, 10))
    }))
  }
))
*/


// TRIGGER ON DISTANCE - FUNGUJE
//check distance for closeCube
/*
// const cube2 = spawnBoxX( 7,0,8, 0,0,0, 1,0.4,1)
export class Proximity {
  update() {
    let transform = cube2.getComponent(Transform)
    let dist = distance(transform.position, camera.position)
    if ( dist < 1) {
      let StartPos = new Vector3(8,0,8)
      let EndPos = new Vector3(8,10,8)
      let SpeedUp = 0.5
      //cube.addComponentOrReplace(new utils.Delay(1000, () => {
        cube.addComponentOrReplace(greenMaterial)  
        cube.addComponentOrReplace(new utils.MoveTransformComponent(StartPos, EndPos, SpeedUp))
      //}))  
      cube.addComponentOrReplace(new utils.Delay(SpeedUp*1000, () => {
        cube.addComponentOrReplace(new utils.MoveTransformComponent(EndPos, StartPos, 10))
      }))
    }
  }
}

engine.addSystem(new Proximity())

// Object that tracks user position and rotation
const camera = Camera.instance

// Get distance
function distance(pos1: Vector3, pos2: Vector3): number {
  const a = pos1.x - pos2.x
  const b = pos1.z - pos2.z
  return a * a + b * b
}
*/

//let rabbit1Shape = new GLTFShape('models/rabbit1/rabbit1.glb')
//let rabbit1 = spawnGltfX(rabbit1Shape, 6,0,6,	0,0,0, 0.01,0.01,0.01)  


// const hud:BuilderHUD =  new BuilderHUD()
// hud.setDefaultParent(scene)
// hud.attachToEntity(cube)
// hud.attachToEntity(rabbit1)
